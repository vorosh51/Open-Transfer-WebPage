### 27.3.2023: EOSC, Brussel

Mrs Ute Gunsenheimer is a secretary general; she received the German delegation (us). After a short round of introductions, Mrs Gunsenheim explained the function of the EOSC in the context of OpenData.

Shortly about EOSC (https://eosc.eu/): 
EOSC is a lobbying organ in the niche between academic institutions and European Commission. They focus on open science, particularly open-access publications and FAIR data. 

From the Q&A with Mrs Ute Gunsenheimer's talk, we understood the following: 
- EOSC is a private fund that feeds from the membership fee. Members are public institutions and universities
- EOSC interviews/talks to/surveys partners (public institutions and universities) and combines common needs and preferences for funding in the scope of open science. EOSC communicate them-self as speaking for the public
- EOSC communicates and lobbies those needs and preferences to European Commission, so European Commission can make community-drives policies and relayed calls proposals. EOSC can even help European Commission to draft those policies and calls for proposals.
- EOSC has a soft power of developed network and collaboration. 
- EOSC has different task [forces](https://eosc.eu/eosc-task-forces) regarding Metadata and data quality, Research careers and curricula, Technical challenges, Sustaining EOSC

One can read more about EOSC objectives and vision in the white paper (attached in our files )

The main idea of open and FAIR initiatives is the recyclability of information. It means that everyone or partners can recycle the results of others and save resources by it. We were told that the industry ([NFDI](https://www.nfdi.de/?lang=en)) invested 1 billion into FAIR data infrastructure as well as COVID vaccine discovery would only be possible with data sharing in a FAIR way 
We understood that FAIR data and open access initiatives require many investments now and are believed to bring reruns many years later. 

We got the impression that the EU is going towards open science, and it is both a down-top and top-down movement. We learned that the European Commission is allocating around a billion eur for open-science projects and FAIR data in the following few years. 

Useful incentives, numbers and links:
EOSC has connections to:
- [GAIA-X](https://gaia-x.eu/):  an initiative that develops, based on European values, a digital governance that can be applied to any existing cloud/ edge technology stack to obtain transparency, controllability, portability and interoperability across data and services.
- [NFDI](https://www.nfdi.de/?lang=en): National Research Data Infrastructure Germany (NFDI), valuable data from science and research are systematically accessed, networked and made usable in a sustainable and qualitative manner for the entire German science system. The example was that industry is also invests into FAIR (not open) data to foster collaborations and safe money. 
- [RDA](https://www.rd-alliance.org/): Research Data Alliance is a global organization with over 12,800 members from 148 countries, and is built on principles that include openness, inclusivity and transparency.  
-  [DSSC.eu](https://dssc.eu/). Data Spaces Support Centre explores the needs of data space initiatives, define common requirements and establish best practices to accelerate the formation of sovereign data spaces as a crucial element of digital transformation in all areas.
- [BMBF FAIR Data Spaces](https://websites.fraunhofer.de/fair-ds/)A joint cloud-based data space for industry and science
- [GAIA-X](https://www.data-infrastructure.eu/GAIAX/Navigation/EN/Home/home.html): It has a goal is a secure and federated data infrastructure that stands for European values, digital sovereignty of the data owners, interoperability of different platform and open source. Within this ecosystem, it will be possible to provide, share, and use data within a trustworthy environment. Thus, spurring innovation and creating added value for the data economy to all who share data.
- [TIGER project](https://www.rd-alliance.org/european-support-rda-returns-roar-rda-tiger-new-support-initiative-rda-working-groups): Research Data Alliance facilitation of Targeted International working Groups for EOSC-related Research solutions (RDA TIGER) (kind of a support platform)

Some more text from Vladimir
